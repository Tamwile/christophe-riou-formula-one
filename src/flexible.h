#ifndef FLEXIBLE_H_
# define FLEXIBLE_H_

struct map_case;
struct vector2;

int same_vector2(struct vector2 v1, struct vector2 v2, float eps);
int same_map_case(struct map_case v1, struct map_case v2);
struct map_case convert_vect(struct vector2 v2);
struct vector2 convert_vect2(struct map_case mc);
struct map_case sum_vect(struct map_case v1, struct map_case v2);

#endif /* FLEXIBLE_H_ */
