#ifndef PATH_H_
# define PATH_H_

struct map_case
{
  int x;
  int y;  
};

struct path
{
  int *x;
  int *y;
  unsigned size;
  unsigned cap;
};

struct ways;

struct path *init_path(void);
struct path *add_path(struct path *p, struct map_case pt);
void set_last(struct ways *w, unsigned ite, struct map_case mc,
	      unsigned weight);
int in_path(struct path *p, struct map_case pt);
void delete_path(struct path *p);

#endif /* PATH_H_ */
