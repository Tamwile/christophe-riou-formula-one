#ifndef WAYS_H
# define WAYS_H

struct ways
{
  unsigned *weight;
  struct path *path;
  struct path *last;
  unsigned size;
  unsigned cap;
};

struct ways *init_ways(void);
void set_last_path(struct ways *w, unsigned ite, struct map_case mc,
	      unsigned weight);
struct ways *add_ways(struct ways *w, struct map_case mp, struct map_case last,
		      unsigned weight);
void delete_ways(struct ways *w);

#endif /* WAYS_H */
