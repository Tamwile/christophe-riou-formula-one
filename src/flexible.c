#include "control.h"
#include "path.h"
#include "flexible.h"

/**
 * => Bool
 * 1 if same vector(position), else 0
 */
int same_vector2(struct vector2 v1, struct vector2 v2, float eps)
{
  if (v1.x <= v2.x + eps && v1.x >= v2.x - eps
      && v1.y <= v2.y + eps && v1.y >= v2.y + eps)
    return 1;
  return 0;
}

int same_map_case(struct map_case v1, struct map_case v2)
{
  if (v1.x == v2.x && v1.y == v2.y)
    return 1;
  return 0;
}


struct map_case convert_vect(struct vector2 v2)
{
  struct map_case v =
  {
    .x = v2.x,
    .y = v2.y
  };
  return v;
}

struct vector2 convert_vect2(struct map_case mc)
{
  struct vector2 v =
  {
    .x = mc.x,
    .y = mc.y
  };
  return v;
}

struct map_case sum_vect(struct map_case v1, struct map_case v2)
{
  struct map_case v;
  v.x = v1.x + v2.x;
  v.y = v1.y + v2.y;
  return v;
}

