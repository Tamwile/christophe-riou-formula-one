#include <stdlib.h>

#include "path.h"
#include "ways.h"
#include "flexible.h"
#include "control.h"
#include "checkpoint.h"

static struct map_case get_case(struct path *p, unsigned ite)
{
  return create_case((p->x)[ite], (p->y)[ite]);
}

struct map_case pop_path(struct path *p)
{
  p->size -= 1;
  return create_case((p->x)[p->size], (p->y)[p->size]);
}

struct map_case pop_path_inv(struct path *p)
{
  struct map_case out = create_case((p->x)[0], (p->y)[0]);
  p->size -= 1;

  int tmp = p->size;
  for (int ite = 1; ite < tmp + 1; ite++)
  {
    (p->x)[ite - 1] = (p->x)[ite];
    (p->y)[ite - 1] = (p->y)[ite];
  }
  return out;
}

struct map_case create_case(int x, int y)
{
  struct map_case out =
  {
    .x = x,
    .y = y
  };
  return out;
}

static void optimise(struct map *m, struct ways *done, struct map_case mc,
	      unsigned mini)
{
  int decal[4] =
  {
    1, 0, -1, 0
  };
  
  for (unsigned ite = 0; ite < 4; ite++)
  {
    int i = decal[ite];
    int j = decal[(ite + 1) % 4];
    if (mc.x + i < m->width && mc.x + i >= 0
	&& mc.y + j < m->height && mc.y + j >= 0)
    {
      struct map_case new = create_case(i, j);
      new = sum_vect(mc, new);
      int index = in_path(done->path, new);
      unsigned wght = ROAD_MALUS;
      if (map_get_floor(m, new.x, new.y) == GRASS)
	wght += GRASS_MALUS;
      if (index != -1 && mini + wght < (done->weight)[index])
      {
	set_last_path(done, in_path(done->path, new), mc, mini + wght);
	optimise(m, done, new, mini + wght);
      }
    }
  }
}

static int find_path_start(struct ways *done, struct path *work,
			   int *init, struct map_case *mc)
{
  if (!work->size)
    return 0;

  *mc = pop_path_inv(work);
  
  if (!done->size)
  {
    *init = 1;
    add_ways(done, *mc, *mc, 0);
  } 
  return 1;
}

static int find_path(struct map *m, struct ways *done, struct path *work)
{
  int decal[4] =
  {
    1, 0, -1, 0
  };
  struct map_case mc;
  int init = 0;
  if (!find_path_start(done, work, &init, &mc))
    return 0;
  unsigned ind = 0;
  unsigned mini = 0;
  int first = 1;
  for (unsigned ite = 0; ite < 4; ite++)
  {
    int i = decal[ite];
    int j = decal[(ite + 1) % 4];
    if (mc.x + i < m->width && mc.x + i >= 0
	&& mc.y + j < m->height && mc.y + j >= 0)
    {
      struct map_case new = create_case(i, j);
      new = sum_vect(mc, new);
      int index = in_path(done->path, new);
      if (in_path(work, new) == -1 && !(map_get_floor(m, new.x, new.y) == BLOCK)
	  && index == -1)
        add_path(work, new);

      if (index != -1)
      {
	if (mini < (done->weight)[index])
	{
	  mini = (done->weight)[index];
	  ind = index;
	}
	  
	if (first)
	{
	  first = 0;
	  mini = (done->weight)[index];
	  ind = index;
	}
      }
    }
  }
  
  mini += ROAD_MALUS;
  if (map_get_floor(m, mc.x, mc.y) == GRASS)
    mini += GRASS_MALUS;

  if (!init)
    add_ways(done, mc, get_case(done->path, ind), mini);

  optimise(m, done, mc, mini);
  return 1;
}

static struct path *create_path(struct map *m, struct ways *w,
				struct map_case mc)
{
  struct map_case l;
  for (int i = 0; i < m->width; i++)
  {
    for (int j = 0; j < m->height; j++)
    {
      if (map_get_floor(m, i, j) == FINISH)
      {
	l.x = i;
	l.y = j;
      }
    }
  }
  
  struct map_case tmp = l;
  struct path *p = add_path(NULL, tmp);
  unsigned ind = in_path( w->path, get_case(w->last, in_path(w->path, l)));

  while (!same_map_case(mc, tmp))
  {
    tmp = get_case(w->path, ind);
    p = add_path(p, tmp);
    ind = in_path(w->path, get_case(w->last, ind));
  }

  return p;
}

/*
 *static void print_weight(struct map *m, struct ways *w)
 *{
 *for (int i = 1; i < m->width - 1; i++)
 * {
 *   for (int j = 1; j < m->height - 1; j++)
 *  {
 *    if (map_get_floor(m, i, j) == BLOCK)
 *	printf("|%s|", "##");
 *    else
 *    {
 *	unsigned ind = in_path(w->path, create_case(i, j));
 *	printf("|%d|", (w->weight)[ind]);
 *    }
 *  }
 *  printf("\n");
 *}
 *}

 *static void print_way(struct map *m, struct path *w)
 *{
 * for (int i = 1; i < m->width - 1; i++)
 *{
 *  for (int j = 1; j < m->height - 1; j++)
 *  {
 *    if (map_get_floor(m, i, j) == BLOCK)
 *	printf("|%s|", "##");
 *    else
 *    {
 *	int out = in_path(w, create_case(i, j));
 *	if (out != -1)
 *	  printf("|%s|", "00");
 *	else
 *	  printf("|%s|", "--");
 *    }
 *  }
 *  printf("\n");
 *}
 *}
 */

struct path *checkpoint(struct map *m, struct map_case pos)
{
  struct ways *done = init_ways();
  struct path *work = add_path(NULL, pos);

  do {
    continue;
  } while (find_path(m, done, work));

  delete_path(work);
  
  struct path *p = create_path(m, done, pos);
  
  delete_ways(done);
  
  return p;
}
