
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "smartcar.h"
#include "control.h"
#include "path.h"

static float modulo(float a, float mod)
{
  while (a > -mod / 2)
    a -= mod;
  while (a <= mod / 2)
    a += mod;
  a -= mod;
  return a;
}

static float module(struct vector2 v)
{
  return sqrtf(v.x * v.x + v.y * v.y + 0.001f);
}

static int accelerate(struct car *car, int dir)
{
  float speed = module(car->speed);
  /*
   *struct vector2 dist;
   *dist.x = goal.x - (car->position).x;
   *dist.y = goal.y - (car->position).y;
   *float distance = module(dist);
   */
  
  if (speed > 5 * CAR_MIN_SPEED)
  {
    if (dir == TURN_LEFT)
      return BRAKE_AND_TURN_LEFT;
    if (dir == TURN_RIGHT)
      return BRAKE_AND_TURN_RIGHT;
    return BRAKE;
  }

  if (dir == TURN_LEFT)
    return ACCELERATE_AND_TURN_LEFT;
  if (dir == TURN_RIGHT)
    return ACCELERATE_AND_TURN_RIGHT;
  return ACCELERATE;
}

static int brake(struct car *car, int dir)
{
  float speed = module(car->speed);

  if (speed > 2 * CAR_MIN_SPEED)
  {
    if (dir == TURN_LEFT)
      return BRAKE_AND_TURN_LEFT;
    if (dir == TURN_RIGHT)
      return BRAKE_AND_TURN_RIGHT;
    return BRAKE;
  }

  return dir;
}

static int turn_toward_cp(struct car *car, struct vector2 cp)
{
  float ordy = cp.y + 0.5 - car->position.y;
  float absx = cp.x + 0.5 - car->position.x;
  float theta = atan((ordy / absx));
  float angle = modulo(theta - car->direction_angle, 2 * M_PI);

  if (angle > -EPS1 && angle < -EPS2)
    return accelerate(car, TURN_LEFT);
  
  else if (angle < EPS1 && angle > EPS2)
    return accelerate(car, TURN_RIGHT);
  
  if (angle < -EPS1)
    return brake(car, TURN_LEFT);
  else if (angle > EPS1)
    return brake(car, TURN_RIGHT);

  return accelerate(car, DO_NOTHING);
}

int dead_end(struct car *car, struct vector2 c, int last)
{
  int out = turn_toward_cp(car, c);

  if (out == TURN_LEFT && last == TURN_RIGHT)
    return ACCELERATE;
  if (out == TURN_RIGHT && last == TURN_LEFT)
    return ACCELERATE;

  if (out == BRAKE_AND_TURN_LEFT && last == BRAKE_AND_TURN_RIGHT)
    return DO_NOTHING;
  if (out == BRAKE_AND_TURN_RIGHT && last == BRAKE_AND_TURN_LEFT)
    return DO_NOTHING;

  if (out == ACCELERATE_AND_TURN_LEFT && last == ACCELERATE_AND_TURN_RIGHT)
    return ACCELERATE;
  if (out == ACCELERATE_AND_TURN_RIGHT && last == ACCELERATE_AND_TURN_LEFT)
    return ACCELERATE;
  
  return out;
}
