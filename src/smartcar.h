#ifndef SMARTCAR_H
# define SMARTCAR_H

# define EPS1 8 * CAR_TURN_ANGLE
# define EPS2 3 * CAR_TURN_ANGLE

struct car;
struct vector2;

int dead_end(struct car *car, struct vector2 c, int last);

#endif /* SMARTCAR_H */
