#include <stdlib.h>

#include "checkpoint.h"
#include "flexible.h"
#include "path.h"

struct path *init_path(void)
{
  struct path *p = malloc(sizeof (struct path));
  if (!p)
    return NULL;

  p->x = malloc(sizeof (int) * 8);
  p->y = malloc(sizeof (int) * 8);
  p->size = 0;
  p->cap = 8;

  return p;
}

struct path *add_path(struct path *p, struct map_case pt)
{
  if (!p)
    p = init_path();

  if (p->size >= p->cap)
  {
    p->x = realloc(p->x, sizeof (int) * (p->cap + 8));
    p->y = realloc(p->y, sizeof (int) * (p->cap + 8));
    p->cap += 8;
  }
  (p->x)[p->size] = pt.x;
  (p->y)[p->size] = pt.y;
  p->size += 1;
  return p;
}

int in_path(struct path *p, struct map_case pt)
{
  if (!p)
    return 0;
  
  for (unsigned ite = 0; ite < p->size; ite++)
  {      
    if (same_map_case(create_case((p->x)[ite], (p->y)[ite]), pt))
      return ite;
  }
  return -1;
}

void delete_path(struct path *p)
{
  if (!p)
    return;
  free(p->x);
  free(p->y);
  free(p);
}

/*
 *void print_path(struct path *p)
 *{
 *  if (!p)
 *  return;
 *for (unsigned ite = 0; ite < p->size; ite++)
 *{
 *  printf("(%d, %d)", (p->x)[ite], (p->y)[ite]);
 *}
 *printf("\n");
 *}
 */
