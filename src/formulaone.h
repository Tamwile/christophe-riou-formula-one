#ifndef FORMULAONE_H_
# define FORMULAONE_H_

struct info
{
  struct path *path;
  struct map_case mc;
  struct map_case last;
  int last_mov;
};

#endif /* FORMULAONE_H_ */
