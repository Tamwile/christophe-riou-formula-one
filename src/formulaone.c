#include <stdlib.h>

#include "control.h"
#include "smartcar.h"
#include "path.h"
#include "checkpoint.h"
#include "ways.h"
#include "flexible.h"
#include "formulaone.h"

struct info g_stock =
{
  .path = NULL,
  .last_mov = DO_NOTHING
};

struct map_case finish_case(struct map *m)
{
  struct map_case l;
  for (int i = 0; i < m->width; i++)
  {
    for (int j = 0; j < m->height; j++)
    {
      if (map_get_floor(m, i, j) == FINISH)
      {
	l.x = i;
	l.y = j;
      }
    }
  }
  return l;
}

enum move update(struct car *car)
{
  struct map *m = car->map;
  
  struct map_case car_p = convert_vect(car->position);
  struct map_case mc;
  if (!(g_stock.path) || (!same_map_case(car_p, g_stock.mc)
			  && !same_map_case(car_p, g_stock.last)))
  {
    struct path *p = checkpoint(m, car_p);
    g_stock.path = p;
    if (p->size > 2)
    {
      g_stock.last = pop_path(p);
      mc = pop_path(p);
    }
    else
      mc = finish_case(m);
    
    g_stock.mc = mc;
  }
  
  else
  {
    g_stock.last = g_stock.mc;
    if (g_stock.path->size > 0)
      mc = pop_path(g_stock.path);
    g_stock.mc = mc;
  }
  
  struct vector2 v = convert_vect2(mc);
  g_stock.last_mov = dead_end(car, v, g_stock.last_mov);
  return g_stock.last_mov;
}
