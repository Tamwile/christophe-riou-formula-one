#ifndef CHECKPOINT_H
# define CHECKPOINT_H

# define ROAD_MALUS 1
# define GRASS_MALUS 6 - ROAD_MALUS

struct map;
struct path;

struct map_case pop_path(struct path *p);
struct map_case create_case(int x, int y);
struct path *checkpoint(struct map *m, struct map_case pos);

#endif /* CHECKPOINT_H */
