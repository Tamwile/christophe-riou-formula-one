#include <stdlib.h>

#include "path.h"
#include "ways.h"

struct ways *init_ways(void)
{
  struct ways *w = malloc(sizeof (struct ways));
  if (!w)
    return NULL;

  w->weight = malloc(sizeof (int) * 4);
  w->path = init_path();
  w->last = init_path();
  w->size = 0;
  w->cap = 4;
  
  return w;
}

void set_last_path(struct ways *w, unsigned ite, struct map_case mc,
		     unsigned weight)
{
  struct path *p = w->last;
  (p->x)[ite] = mc.x;
  (p->y)[ite] = mc.y;
  (w->weight)[ite] = weight;
}

struct ways *add_ways(struct ways *w, struct map_case mp, struct map_case last,
		      unsigned weight)
{
  if (!w)
    w = init_ways();

  if (w->size >= w->cap)
  {
    w->weight = realloc(w->weight, sizeof (int) * (w->cap + 8));
    w->cap += 8;
  }
  
  (w->weight)[w->size] = weight;
  w->path = add_path(w->path, mp);
  w->last = add_path(w->last, last);
  w->size += 1;
  return w;
}

void delete_ways(struct ways *w)
{
  if (!w)
    return;
  delete_path(w->path);
  delete_path(w->last);
  free(w->weight);
  free(w);
}
